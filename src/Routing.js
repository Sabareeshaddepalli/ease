/* eslint-disable no-unused-expressions */
import React from 'react'
import { Switch, Route } from 'react-router-dom';
import Register from './components/Register/Register'
import Login from './components/Login/Login'
import Home from './components/HomeApp/Home/Home'


const Routing=()=> {
  return(
   <Switch>
     <Route path='/' exact component={Login} />
     <Route path='/register' component={Register} />
      <Route path='/home' component={Home} />
   </Switch>
 )
}
export default Routing
