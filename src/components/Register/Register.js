import React, { useState } from "react";
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import LinearProgress from '@material-ui/core/LinearProgress';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import './Register.css';


function KeyboardDatePicker() {
  const dudUrl = '/';
  const [load, setloding] = useState(false);
  const [formEmpty, formFilled] = useState({
    name: '',
    email: '',
    password: '',
    orgId:'',
    showPassword: false
  });

  const handleClickShowPassword = () => {
    formFilled({ ...formEmpty, showPassword: !formEmpty.showPassword });
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  const fillForm = name => event => {
    formFilled({ ...formEmpty, [name]: event.target.value });
  };

  const clearForm = () => {
    formFilled({
      name: '',
      email: '',
      password: '',
      orgId:'',
      showPassword: false
    })
  }


  const register = () => {
    setloding(true)
    console.log(formEmpty)
  }

  return (
    <div className="body">
      <div className="contain">
        {load ? <LinearProgress color="secondary" className="loading" /> : null}
        <Paper elevation={8}>
          <Grid container spacing={0}>
            <Grid item xs={7} className="leftImage">
            </Grid>
            <Grid className="form" item xs={5}>
              <h2 className="heading">Register</h2>
              <FormControl className="inputSize">
                <InputLabel htmlFor="name">Name</InputLabel>
                <Input id="name" value={formEmpty.name} onChange={fillForm('name')} />
              </FormControl>
              <FormControl className="inputSize">
                <InputLabel htmlFor="orgId">OrgId</InputLabel>
                <Input id="orgId" value={formEmpty.orgId} onChange={fillForm('orgId')} />
              </FormControl>
              <FormControl className="inputSize">
                <InputLabel htmlFor="email">Email</InputLabel>
                <Input id="email" value={formEmpty.email} onChange={fillForm('email')} />
              </FormControl>
              <FormControl className="inputSize">
                <InputLabel htmlFor="password">Create Password</InputLabel>
                <Input
                  id="password"
                  type={formEmpty.showPassword ? 'text' : 'password'}
                  value={formEmpty.password}
                  onChange={fillForm('password')}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword} >
                        {formEmpty.showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>
              <br />
              <div className="btnGroup">
                <Button disabled={load} variant="contained" color="primary" onClick={register} className="button" >Register </Button>
                <Button disabled={load} variant="contained" color="secondary" className="button" onClick={clearForm}>Cancel</Button>
              </div>
              <div className="link">
                <Link to={dudUrl}  >
                  Already have Account ?
           </Link>
              </div>
            </Grid>
          </Grid>
        </Paper>
      </div>
    </div>
  );
}

export default KeyboardDatePicker;
