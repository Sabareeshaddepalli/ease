import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import styles from './Admin.module.css'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import AccessAlarmIcon from '@material-ui/icons/AccessAlarm';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';


const useStyles = makeStyles(theme => ({
  root: {
    padding: '20px 20px 10px 20px',
    backgroundColor: 'white'
  },
  grid: {
    flexGrow: 1,
  },
  control: {
    padding: theme.spacing(5),
  },
}));

const Admin = () => {
  const classes = useStyles();
  return (
    <div className={styles.mainBody}>
      <Grid container className={classes.grid} spacing={5}>
        <Grid item xs={3}>
          <Paper className={classes.root} >
            <Paper className={styles.heading} elevation={8}>
              Heading
        </Paper>
            <Typography variant="h6" component="h5">
              This is a sheet of paper.
        </Typography>
            <Divider />
            <ListItem className={styles.list}>
              <ListItemIcon> <AccessAlarmIcon/></ListItemIcon>
              <ListItemText primary="Updated 1 hour ago" />
            </ListItem>

          </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.root} >
            <Paper className={styles.heading} elevation={8}>
              Heading
        </Paper>
            <Typography variant="h6" component="h5">
              This is a sheet of paper.
        </Typography>
            <Divider />
            <ListItem className={styles.list}>
              <ListItemIcon> <AccessAlarmIcon/></ListItemIcon>
              <ListItemText primary="Updated 1 hour ago" />
            </ListItem>
          </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.root} >
            <Paper className={styles.heading} elevation={8}>
              Heading
        </Paper>
            <Typography variant="h6" component="h5">
              This is a sheet of paper.
        </Typography>
            <Divider />
            <ListItem className={styles.list}>
              <ListItemIcon> <AccessAlarmIcon/></ListItemIcon>
              <ListItemText primary="Updated 1 hour ago" />
            </ListItem>
          </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.root} >
            <Paper className={styles.heading} elevation={8}>
              Heading
        </Paper>
            <Typography variant="h6" component="h5">
              This is a sheet of paper.
        </Typography>
            <Divider />
            <ListItem className={styles.list}>
              <ListItemIcon> <AccessAlarmIcon/></ListItemIcon>
              <ListItemText primary="Updated 1 hour ago" />
            </ListItem>
          </Paper>
        </Grid>
      </Grid>

    </div>
  )
}

export default Admin
