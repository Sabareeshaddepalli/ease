import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Slide from '@material-ui/core/Slide';

const useStyles = makeStyles(theme => ({
  card: {
    display: 'flex',
    marginBottom: '20px'
  },
  details: {
    display: 'flex',
    flexDirection: 'row',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
}));

export default function RunningProjects() {

  const classes = useStyles();
  const checked = true;
  let array = [1, 2, 3, 4, 5, 6]
  const projects = array.map((x) => {
    return (
      <Grid item xs={6} key={x}>
        <Slide direction="up" in={checked} style={{ transformOrigin: '0 0 0' }}
          {...(checked ? { timeout: 500 } : {})} mountOnEnter unmountOnExit>
          <Card className={classes.card} key={x}>
            <div className={classes.details}>
              <CardMedia
                component="img"
                alt="Contemplative Reptile"
                height="220"
                image="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6Vd3IeORTobMOcg9wHUtDCyBiXxp_TC82m9XICNpB_tLmQcXY"
                title="Contemplative Reptile"
              />
              <CardContent className={classes.content}>
                <Typography component="h5" variant="h5">
                  Lorem Ipsum
          </Typography>
                <Typography variant="subtitle1" color="textSecondary">
                  Neque porro quisquam est qui velit..
          </Typography>
              </CardContent>
            </div>
          </Card>
        </Slide>
      </Grid>
    )
  })


  return (
    <div>
      <Grid container spacing={3}>
        {projects}
      </Grid>
    </div>
  );
}