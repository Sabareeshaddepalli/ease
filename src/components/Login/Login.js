import React, { useState } from "react";
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import LinearProgress from '@material-ui/core/LinearProgress';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import FormGroup from '@material-ui/core/FormGroup';
import { Link} from 'react-router-dom';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import './Login.css'


const Login = (props) => {
  const dudUrl = '/register'
  const [form, setform] = useState({
    email: '',
    password: '',
    showPassword: false,
  });

  const [load, setloding] = useState(false);

  const handleChange = name => event => {
    setform({ ...form, [name]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setform({ ...form, showPassword: !form.showPassword });
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  const login = () => {
    setloding(true)
    props.history.push('/home')
  }

  const clearForm = () => {
    setform({
      email: '',
      password: '',
      showPassword: false,
    })
  }

 
  

  return (
    <div className="Loginbody">
      <div className="box">
        {load ? <LinearProgress color="secondary" className="loading" /> : null}
        <Paper elevation={8} className="loginForm">
          <h2 className="heading">Login</h2>
          <FormControl className="inputSize">
            <InputLabel htmlFor="email">Email</InputLabel>
            <Input id="email" value={form.email} onChange={handleChange('email')} />
          </FormControl>
          <FormControl className="inputSize">
            <InputLabel htmlFor="password" >Password</InputLabel>
            <Input
              id="password"
              type={form.showPassword ? 'text' : 'password'}
              value={form.password}
              onChange={handleChange('password')}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword} >
                    {form.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl> <br /><br />
          <FormGroup >
            <FormControlLabel
              control={
                <Checkbox  value="checkedA" />
              }
              label="Remember me"
            />
          </FormGroup>
          <div className="btnGrup">
            <Button disabled={load} variant="contained" color="primary" onClick={login} className="button" >Login </Button>
            <Button disabled={load} variant="contained" color="secondary" className="button" onClick={clearForm}>Cancel</Button>
          </div>
          <br />
          <div className="link">
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <Link to={dudUrl}>Forgot Password!</Link>
              </Grid>
              <Grid item xs={6}>
                <Link to={dudUrl}>New User?</Link>
              </Grid>
            </Grid>
          </div>
        </Paper>
      </div>
    </div>
  );
};




export default Login;
